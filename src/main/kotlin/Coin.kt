@JvmInline
value class Coin(val amount: Int)

@JvmInline
value class Quantity(val value: Int)
