class CoinChanger
{
    companion object
    {
        private val availableCoins = listOf(
            Coin(10),
            Coin(5),
            Coin(2),
            Coin(1)
        )

        fun change(amount: Int): List<Pair<Coin, Quantity>>
        {
            val changes = mutableListOf<Pair<Coin, Quantity>>()
            var rest = amount
            availableCoins.forEach { coin ->
                if (rest > 0 && coin.amount <= rest)
                {
                    val quotient = Quantity(rest / coin.amount)
                    changes.add(coin to quotient)
                    rest -= (coin.amount * quotient.value)
                }
            }

            return changes

//            if (amount == 3)
//            {
//                return listOf(
//                    Coin(2) to Quantity(1),
//                    Coin(1) to Quantity(1)
//                )
//            }
//            return listOf(Coin(amount) to Quantity(1))
        }
    }
}
