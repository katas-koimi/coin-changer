import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class ChangerTest
{
    @Test
    fun `one euro change`()
    {
        assertEquals(listOf(Coin(1) to Quantity(1)), CoinChanger.change(1))
    }

    @Test
    fun `ten euros change`()
    {
        assertEquals(listOf(Coin(10) to Quantity(1)), CoinChanger.change(10))
    }

    @Test
    fun `three euros change`()
    {
        assertEquals(
            listOf(
                 Coin(2) to Quantity(1),
                 Coin(1) to Quantity(1)
            ),
            CoinChanger.change(3))
    }

    @Test
    fun `seven euros change`()
    {
        assertEquals(
            listOf(
                 Coin(5) to Quantity(1),
                 Coin(2) to Quantity(1)
            ),
            CoinChanger.change(7))
    }

    @Test
    fun `fifty three euros change`()
    {
        assertEquals(
            listOf(
                 Coin(10) to Quantity(5),
                 Coin(2) to Quantity(1),
                 Coin(1) to Quantity(1),
            ),
            CoinChanger.change(53))
    }
}